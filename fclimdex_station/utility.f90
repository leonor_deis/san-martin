! subroutines used by fclimdex_station
!
! ----------------------------------------------------------------
! check if sub_dir exists, if not, create it.
!
      subroutine check_folders
      use COMM
      character(230) :: command,tempf
      integer        :: status,i,system,n,n0(N_index)

! Will create folders ONLY for standard and user-requested indices
      n=N_index-3
      do i=1,n
         n0(i)=i 
      enddo

      if(save_thresholds)then
        do i=38,39
          n=n+1
          n0(n)=i
        enddo
      endif

      if(cal_EHI)then
        do i=40,40
          n=n+1
          n0(n)=i
        enddo
      endif

      if(OS=='windows') then       ! on Windows system
      	sub=sub2(2)
        do i=1,n
           tempf=trim(opt_dir)//trim(folder(n0(i)))
           if(opt_dir(2:2)==':') then
      	      command='if not exist '//trim(tempf)//sub//'nul '//opt_dir(1:2)//' && cd '//trim(opt_dir(3:))//' && mkdir '//trim(folder(n0(i)))
           else
      	      command='if not exist '//trim(tempf)//sub//'nul'//' cd '//trim(opt_dir)//' && mkdir '//trim(folder(n0(i)))
           endif
           status= system(command)
         enddo
      endif

      if(OS=='linux') then        ! on Linux and Mac system
      	sub=sub2(1)
        do i=1,n
            tempf=trim(opt_dir)//trim(folder(n0(i)))
            command='if [ ! -d '//trim(tempf)//' ]; then cd '//trim(opt_dir)//'; mkdir '//trim(folder(n0(i)))//'; fi'
            status= system(command)
        enddo
      endif
      
      
    print*,' folders checked ...'
      
      return
      end subroutine check_folders


! -------------------------------------------------------------------------
! Read input parameters of each station, and file name.
! n is record #, starting from 1.
! If end-of-file, then set iend=1
!
subroutine read_file(n)
  use comm
  integer      :: n
  character*80 :: header
  logical      :: exist 

    iend=0
    if(n==1)then  ! to open input file
       inquire(file=trim(ipt_dir)//trim(para_file),exist=exist)
       if(.not.exist) stop 'Error in input.nml: para_file does not exist'
       inquire(file=trim(ipt_dir)//trim(inf_file),exist=exist)
       if(.not.exist) stop 'Error in input.nml: inf_file does not exist'
       call getID(ID_para)
       open (ID_para, file=trim(ipt_dir)//trim(para_file),action='read')
       read (ID_para, '(a80)') header
       call getID(ID_inf)
       open (ID_inf, file=trim(ipt_dir)//trim(inf_file),action='read')
    endif

     read (ID_para, '(a20,f10.2,3i6,i10)',end=100) StnID, LATITUDE, &
               STDSPAN, BASESYEAR, BASEEYEAR, PRCPNN
     print*,trim(adjustl(StnID)),STDSPAN,BASESYEAR,BASEEYEAR,PRCPNN
     Oname=trim(adjustl(StnID))//'_'
     read (ID_inf, '(a80)', end=100) ifile
     if(trim(ifile).eq." ") then
       print*, "Read in data filename ERROR happen in:"
       print*, "infilename.txt, line:", n
       stop
     endif
     return

100   iend=1
      print*,'finish reading input file, now to stop ....'
      close(ID_para)
      close(ID_inf)
      return

end subroutine read_file


!-----------------------------------------------------------------------
! Calculate percentile
!
      subroutine percentile(x, length, nl, per, oout)
      use COMM
      use functions
      integer :: length,nl    ! elements # of x and per
      real(DP),dimension(length):: x,xtos    ! input array, and its non-MISSING part
      real(DP),dimension(nl)    :: per,oout  ! percentile level and results
      real(DP):: bb,cc ! temporary variable
      integer :: nn,i  ! loop index and count #

! check if percentile level is out of bound.
      if(maxval(per)>1..or.minval(per)<0.) stop 'ERROR in percentile: per out of range [0.,1.] !'

! choose non-MISSING input, but is it necessary? I think x is already chosen ......
      nn=0
      do i=1, length
        if(nomiss(x(i)))then
          nn=nn+1
          xtos(nn)=x(i)
        endif
      enddo

      if(nn.eq.0) then
        oout=MISSING
      else
        call sort(nn,xtos) ! ascending numerical order
        do i=1,nl
          bb=nn*per(i)+per(i)/3.+1./3.
          cc=real(int(bb))
          if(int(cc).ge.nn) then
            oout(i)=xtos(nn)
          else
            oout(i)=xtos(int(cc))+(bb-cc)*	 &
               (xtos(int(cc)+1)-xtos(int(cc)))
          endif
        enddo
      endif

      return
      end subroutine percentile


!-----------------------------------------------------------------------
!  Sort an array arr(1:n) into ascending numerical order using the Quicksort algorithm.
!    n is element # of input array; arr is replace on output by its sorted rearrangement.
!    Parameters: M is the size of subarrays sorted by straight insertion
!    and NSTACK is the required auxiliary.
!  from Numerical Recipes.
!
! need to re-write using F90 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
      SUBROUTINE sort(n,arr)
      use COMM !, only:DP
      INTEGER, PARAMETER :: M=7,NSTACK=50
      INTEGER  :: n, i,ir,j,jstack,k,l,istack(NSTACK)
      REAL(DP) :: arr(n)
      REAL(DP) :: a,temp

      jstack=0
      l=1
      ir=n
1     if(ir-l.lt.M)then
        do 12 j=l+1,ir
          a=arr(j)
          do 11 i=j-1,1,-1
            if(arr(i).le.a)goto 2
            arr(i+1)=arr(i)
11        continue
          i=0
2         arr(i+1)=a
12      continue
        if(jstack.eq.0)return
        ir=istack(jstack)
        l=istack(jstack-1)
        jstack=jstack-2
      else
        k=(l+ir)/2
        temp=arr(k)
        arr(k)=arr(l+1)
        arr(l+1)=temp
        if(arr(l+1).gt.arr(ir))then
          temp=arr(l+1)
          arr(l+1)=arr(ir)
          arr(ir)=temp
        endif
        if(arr(l).gt.arr(ir))then
          temp=arr(l)
          arr(l)=arr(ir)
          arr(ir)=temp
        endif
        if(arr(l+1).gt.arr(l))then
          temp=arr(l+1)
          arr(l+1)=arr(l)
          arr(l)=temp
        endif
        i=l+1
        j=ir
        a=arr(l)
3       continue
          i=i+1
        if(arr(i).lt.a)goto 3
4       continue
          j=j-1
        if(arr(j).gt.a)goto 4
        if(j.lt.i)goto 5
        temp=arr(i)
        arr(i)=arr(j)
        arr(j)=temp
        goto 3
5       arr(l)=arr(j)
        arr(j)=a
        jstack=jstack+2
        if(jstack.gt.NSTACK)pause 'NSTACK too small in sort'
        if(ir-i+1.ge.j-l)then
          istack(jstack)=ir
          istack(jstack-1)=i
          ir=j-1
        else
          istack(jstack)=j-1
          istack(jstack-1)=l
          l=i
        endif
      endif
      goto 1
      END SUBROUTINE sort



!-----------------------------------------------------------------------
! Calculate threshold
!
      subroutine threshold(idata, lev, nl, odata, flg)
      use COMM
      use functions
      integer  :: flg,  &    ! overflow indicator (1 = overflow)
                  nl         ! element # of lev
      real(DP) :: idata(BYRS,DoY+2*SS), &   ! input array - extended data in base period
                odata(DoY,nl), &            ! output 
                lev(nl)                     ! input percentile level
      real(DP) :: tosort(BYRS*WINSIZE), &   ! selected non-MISSING data for each day covering all base years
                  rtmp(nl)                ! temporary percentile results
      integer  :: nn,i,j,k,Icritical,SS2  ! loop index and/or count #

!      Icritical=int(BYRS*WINSIZE*.85)
      Icritical=int(BYRS*WINSIZE*NoMissingThreshold)
      SS2=2*SS

      do i=1,DoY
! select non-MISSING data for each day covering all base years
        nn=0
        do j=1,BYRS
          do k=i,i+SS2
            if(nomiss(idata(j,k))) then
              nn=nn+1
              tosort(nn)=idata(j,k)
            endif
          enddo
        enddo
        if(nn.lt.Icritical) then   ! overflow !
          flg=1
          return
        endif
        call percentile(tosort,nn,nl,lev,rtmp)  ! get percentile
        odata(i,:)=rtmp(:)
      enddo

      return
      end subroutine threshold


! ---------------------------------------------------------------------------
!  save the 12 monthly and 1 annual index data,
!    removing all consecutive MISSING years at the beginning and end
!
    subroutine save_monthly(k,data)
    use COMM
    real    :: data(YRS,13), a
    integer :: ID, k,i,j,i0,j0

    i0=1        ! find first few years that have all MISSING values
    do
      if(count(data(i0,:)==MISSING)/=13) exit
      i0=i0+1
      if(i0==YRS) stop 'ALL data are MISSING, please check the index'
    enddo

    j0=YRS        ! find last few years that have all MISSING values
    do
      if(count(data(j0,:)==MISSING)/=13) exit
      j0=j0-1
      if(j0==1) stop 'ALL data are MISSING, please check the index'
    enddo

    a=maxval(data)
    if(a>=1.e7) write(*,'(f,a,a)'),a,' is too big for index ',trim(folder(k))

    call getID(ID)
    open(ID,file=ofile(k))
    write(ID,'(a)') "Year    Jan     Feb     Mar     Apr     May     Jun     Jul     Aug     Sep     Oct     Nov     Dec    Annual"

    do i=i0,j0
       write(ID,'(i4,13f8.1)') i+SYEAR-1,(data(i,j),j=1,13)
    enddo
    close(ID)
    
    return
    end subroutine save_monthly


! ------------------------------------------------------------------------------
!  save the annual index data, removing all consecutive MISSING years
!    at the beginning and end
!
    subroutine save_annual(k,data)
    use COMM
    real    :: data(YRS), a
    integer :: ID, k,i,i0,j0

    i0=1        ! find first few years that have all MISSING values
    do
      if(data(i0)/=MISSING) exit
      i0=i0+1
      if(i0==YRS) stop 'ALL data are MISSING, please check the index'
    enddo

    j0=YRS        ! find last few years that have all MISSING values
    do
      if(data(j0)/=MISSING) exit
      j0=j0-1
      if(j0==1) stop 'ALL data are MISSING, please check the index'
    enddo

    a=maxval(data)
    if(a>=1.e7) write(*,'(f,a,a)'),a,' is too big for index ',trim(folder(k))

    call getID(ID)
    open(ID,file=ofile(k))

    if(a>1000.)then
      write(ID,'(a,2x,a)') "Year",trim(folder(k))
    else
      write(ID,'(a,4x,a)') "Year",trim(folder(k))
    endif

    do i=i0,j0
       write(ID,'(i4,f8.1)') i+SYEAR-1,data(i)
    enddo
    close(ID)

    return
    end subroutine save_annual

    

! --------------------------------------------------------------
!  save the Temperature thresholds with DoY elements
!
    subroutine save_thres_Temp(k,dat1,dat2,dat3,dat4,dat5,dat6)
    use COMM
    real,dimension(DoY) :: dat1,dat2,dat3,dat4,dat5,dat6 ! 6 index
    real::     a
    integer :: ID, k,i   ! unit ID, folder #, loop index

    a=maxval((/maxval(dat1),maxval(dat2),maxval(dat3),maxval(dat4),maxval(dat5),maxval(dat6)/))
    if(a>=1.e7) write(*,'(f,a,a)'),a,' is too big in file ',trim(folder(k))

    call getID(ID)
    open(ID,file=ofile(k))
    write(ID,'(a)') "DoY  an10p   an50p   an90p   ax10p   ax50p   ax90p"

    do i=1,DoY
       write(ID,'(i3,6(f8.1))') i,dat1(i),dat2(i),dat3(i),dat4(i),dat5(i),dat6(i)
    enddo
    close(ID)

    return
    end subroutine save_thres_Temp



! --------------------------------------------------------------
!  save the PRCP thresholds with only 1 element
!
    subroutine save_thres_PRCP(k,dat1,dat2,dat3)
    use COMM
    real :: dat1,dat2,dat3 ! 3 index
    real ::  a
    integer :: ID, k   ! unit ID, folder #

    a=maxval((/dat1,dat2,dat3/))
    if(a>=1.e7) write(*,'(f,a,a)'),a,' is too big in file ',trim(folder(k))

    call getID(ID)
    open(ID,file=ofile(k))
    write(ID,'(a)') "  R85p    R95p    R99p"
    write(ID,'(3(f8.1))') dat1,dat2,dat3
    close(ID)

    return
    end subroutine save_thres_PRCP
    

! -----------------------------------------------------------------
! ******* Get a free unit ID for I/O file.
!
	  subroutine getID(ID)
	  integer :: ID,ios
          logical :: lopen

	  ID=20
          do 
              inquire(unit=ID,opened=lopen,iostat=ios)
              if(ios==0.and.(.not.lopen)) return
              ID=ID+1
          enddo

	  return
	  end subroutine getID


! ------------------------------------------------------------------
!  get current time, and output formatted time in log file
!
	subroutine get_time(Iflag,ID,current)
	integer      :: tarr(8),ii,Iflag, ID
        character*23 :: file
        character*45 :: now(3),printf
        double precision :: current
        data now/'Task starts from:','Task ends at:','Current time:'/

        ii=3
        if(Iflag==0) ii=1
        if(Iflag==1) ii=2
	call Date_and_Time(values=tarr)
	write(file,12) tarr(1),tarr(2),tarr(3),tarr(5),tarr(6),tarr(7),tarr(8)
12      format(i4.4,'.',i2.2,'.',i2.2,' ',i2.2,':',i2.2,':',i2.2,'.',i3.3)

       printf=trim(now(ii))//' '//file

        current= tarr(5)*3600.d0+tarr(6)*60.d0+tarr(7)+0.001d0*tarr(8)

	  write(*,'(/,a,/)') printf
	  if(ID > 0)  write(ID,'(/,a,/)') printf

	return
	end subroutine get_time
