How to compile and run the code (use ifort as example):

       ================================

A. on linux: 
 Edit the Makefile (switch on/off the NC_LIB for netcdf file, note OpenMP is not working yet), then
    $ make
 if everything is ok, you'll get an executable file - dls.exe

to run the code, edit the dls_1.1.nml, then
    $ ./dls.exe

To clean temporary files: 
    $ make clean

Although OpenMP is not working now, but
if OpenMP is actually used, you can change processor core number # by setting
    $ export OMP_NUM_THREADS=#
    $ ./dls.exe
by ddefault, it uses all the cores on one machine.


B. on windows machines: (no Netcdf, [] is optional)
    > ifort dls_1.1.f90 out_txt.f90 output2.f90 -o dls.exe [/Qopenmp]
    > [ set OMP_NUM_THREADS=# ]
    > dls.exe

   You can link the netcdf library and compile related files as well.

       ================================

Note:
- the namelist file dls_1.1.nml must be saved in the run directory (i.e., where dls.exe is executed)
- if there is an error like "segmentation fault", edit the .bashrc file and add one line 'ulimit -s unlimited'.